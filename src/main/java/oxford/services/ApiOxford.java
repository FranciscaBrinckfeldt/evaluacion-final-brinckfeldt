package oxford.services;

import java.io.IOException;
import java.util.logging.Logger;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import oxford.model.api.OxfordDef;

import com.google.gson.*;

public class ApiOxford {

    private final Logger log = Logger.getLogger(ApiOxford.class.getName());

    private final String prefixUrl = "https://od-api.oxforddictionaries.com/api/v2/entries/es/";
    private final String suffixUrl = "?fields=definitions&strictMatch=false";
    private OkHttpClient client = new OkHttpClient();
    private Gson gson = new Gson();
    
    public OxfordDef getDefinitions(String palabra) throws IOException {

        String app_id = System.getProperty("OXFORD_APP_ID");
        String app_key = System.getProperty("OXFORD_APP_KEY");
      
        log.info("Request::app_id::" + app_id);
        log.info("Request::app_key::" + app_key);
        log.info("Request::Url" + prefixUrl + palabra + suffixUrl);

        Request request = new Request.Builder()
                                        .url(prefixUrl + palabra + suffixUrl)
                                        .get()
                                        .addHeader("accept", "application/json")
                                        .addHeader("app_id", "c4bca572")
                                        .addHeader("app_key", "3960242709c35b4d4e53d834d7b1dd1c")
                                        .build();

        Response response = client.newCall(request).execute();

        OxfordDef body = gson.fromJson(response.body().string(), OxfordDef.class);

        log.info("Response::" + body.toString());

        return body;
    }

}