package oxford.model.domain;

import java.util.ArrayList;
import java.util.List;

public class HistorialResponse {
    List<ResultResponse> historial = new ArrayList<>();

    public List<ResultResponse> getHistorial() {
        return historial;
    }

    public void setHistorial(List<ResultResponse> historial) {
        this.historial = historial;
    }
    
    
}
