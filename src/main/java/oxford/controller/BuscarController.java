package oxford.controller;

import oxford.model.domain.ResultResponse;
import com.google.gson.Gson;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

@WebServlet(name = "BuscarController", urlPatterns = {"/buscar"})
public class BuscarController extends HttpServlet {

    Gson gson = new Gson();
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("index.jsp").forward(request, response);
        return;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<String> definiciones = new ArrayList<>();
        Client client = ClientBuilder.newClient();
        String palabra = request.getParameter("palabra");
        String localUrl = request.getScheme() + "://"
                        + request.getServerName() + ":"
                        + request.getServerPort()
                        + request.getContextPath();

        WebTarget myResource = client.target(localUrl + "/api/oxford/find/" + palabra);
        ResultResponse res = null;
        try {
            res = myResource.request(MediaType.APPLICATION_JSON).get(ResultResponse.class);
        } catch (NotFoundException e) {
            request.setAttribute("msgerror", "Palabra no existe");
            e.printStackTrace();
            request.getRequestDispatcher("index.jsp").forward(request, response);
            return;
        } catch (Exception e) {
            request.setAttribute("msgerror", "Error en proceso interno del servidor");
            e.printStackTrace();
            request.getRequestDispatcher("index.jsp").forward(request, response);
            return;
        }

        definiciones.addAll(res.getDefiniciones());

        request.setAttribute("msgerror", null);
        request.setAttribute("definiciones", definiciones);
        request.setAttribute("palabra", palabra);
        request.getRequestDispatcher("index.jsp").forward(request, response);
        return;
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
