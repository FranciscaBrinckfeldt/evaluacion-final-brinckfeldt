package oxford.controller;

import oxford.model.domain.HistorialResponse;
import com.google.gson.Gson;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

@WebServlet(name = "HistorialController", urlPatterns = {"/historial"})
public class HistorialController extends HttpServlet {

    Gson gson = new Gson();
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HistorialResponse res = new HistorialResponse();
        Client client = ClientBuilder.newClient();
        String localUrl = request.getScheme() + "://"
                        + request.getServerName() + ":"
                        + request.getServerPort()
                        + request.getContextPath();

        WebTarget myResource = client.target(localUrl + "/api/oxford/history");

        try {
            res = myResource.request(MediaType.APPLICATION_JSON).get(HistorialResponse.class);
        } catch (Exception e) {
            request.setAttribute("msgerror", "Error en proceso interno del servidor");
            e.printStackTrace();
            return;
        }

        request.setAttribute("historial", res.getHistorial());
        request.getRequestDispatcher("historial.jsp").forward(request, response);   
        return;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
