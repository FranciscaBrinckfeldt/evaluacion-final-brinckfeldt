<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="oxford.model.entities.Definicion"%>
<%@page import="oxford.model.dao.DefinicionDAO"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <link rel="stylesheet" href="css/style.css"/>
        <title>Diccionario Oxford - Evaluación Final</title>
        <c:set var="req" value="${pageContext.request}" />
        <c:set var="baseURL" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" />
    </head>
    <body>
        <nav>
            <h4>Diccionario Oxford</h4>
            <div class="buttons">
                <a href="/">Buscar</a> | <a href="/historial">Historial</a>
            </div>
        </nav>
        <section>
            <form method="POST" action="buscar">
                <input name="palabra" placeholder="Busca una definición..." value="${palabra}">
                <button type="submit">Buscar</button>
            </form>
            <c:choose>
                <c:when test="${definiciones.size() > 0}">
                    <div class="container">
                        <h3>Mostrando definiciones para "${palabra}"</h3>
                        <c:forEach var="definicion" items="${definiciones}">
                            <p class="definition">${definicion}</p>
                        </c:forEach>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="no-data">
                        <p>Busca las definiciones de una palabra</p>
                    </div>
                </c:otherwise>
            </c:choose>
        </section>
        <footer>
            <span>Taller Aplicaciones Empresariales - Francisca Brinckfeldt - 2020</span>
        </footer>
    </body>
</html>
