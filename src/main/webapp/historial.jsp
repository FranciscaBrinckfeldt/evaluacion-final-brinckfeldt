<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="oxford.model.entities.Definicion"%>
<%@page import="oxford.model.dao.DefinicionDAO"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <link rel="stylesheet" href="css/style.css"/>
        <title>Diccionario Oxford - Evaluación Final</title>
        <c:set var="req" value="${pageContext.request}" />
        <c:set var="baseURL" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" />
    </head>
    <body>
        <nav>
            <h4>Diccionario Oxford</h4>
            <div class="buttons">
                <a href="/">Buscar</a> | <a href="/historial">Historial</a>
            </div>
        </nav>
        <section>
        <c:choose>
            <c:when test="${historial.size() > 0}">
                <div class="container">
                    <h3>Historial</h3>
                    <c:forEach var="item" items="${historial}">
                        <div class="historial-card">
                            <div class="left">
                                <p class="label">ID</p>
                                <p class="value">${item.getId()}</p>
                                <p class="label">Palabra</p>
                                <p class="value">${item.getPalabra()}</p>
                            </div>
                            <div class="right">
                                <c:forEach var="definition" items="${item.getDefiniciones()}">
                                    <p class="definition">${definition}</p>
                                </c:forEach>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </c:when>
            <c:otherwise>
                <div class="no-data">
                    <p>No hay historial que mostrar</p>
                </div>
            </c:otherwise>
        </c:choose>
        </section>
        <footer>
            <span>Taller Aplicaciones Empresariales - Francisca Brinckfeldt - 2020</span>
        </footer>
    </body>
</html>
